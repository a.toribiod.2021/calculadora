def suma(x, y):
    num = int(x) + int(y)
    return num


def resta(x, y):
    num = int(x) - int(y)
    return num


if __name__ == "__main__":
    suma1 = suma(1, 2)
    suma2 = suma(3, 4)

    print('Las suma de 1 y 2 es:', suma1)
    print('Las suma de 3 y 4 es:', suma2)

    resta1 = resta(6, 5)
    resta2 = resta(8, 7)

    print('La resta de 6 y 5 es:', resta1)
    print('La resta de 8 y 7 es:', resta2)
